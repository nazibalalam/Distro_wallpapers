# Distro_wallpapers
This project is created to collect wallpapers, login and GRUB screens, and other artworks of each and every linux distribution.

# Related project
https://github.com/nazibalalam/Distro_wallpapers-CLI-bash <br>
https://github.com/happyeggchen/Distro_wallpapers-CLI-fish

# Installation
This project is just to collect all Linux distro wallpapers, so you can install (download) these wallpapers using the scripts mentioned above. With them you can browse through these folders withought downloading, download the folders you want, configure nitrogen for these wallpapers automatically and etc. Not all of these features have been implemented yet so you can contribute to these scripts. 

# List of included distro(s)
Last updated = 2021-3-21 [UTC+8]

Currently we have these wallpapers :- 

- Debian [GPL-2.0+]
  - buster

-  Pop_OS! [[Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0)](https://creativecommons.org/licenses/by-sa/4.0/)]

- Zorin-OS [https://zorinos.com/press/#copyright-licensing]
  - 12.4
  - 15.2

- aosp [APACHE2]
  - 10

- bluelake [Greg Annandale Photography Image Licence]
  - 2.3

- deepin [Unknown]
  - 20

- elementary [https://github.com/elementary/wallpapers/blob/master/LICENSE.md]

- gnome [CCBYSA / Public Domain / CC0]
  - 3.30

- lineageos [Maybe MIT]
  - 17.1

- manjaro_xfce [Unknown]
  - 20.2.1

- mint [Unknown]
  - 20.1

- plasma [GPL-2+]
  - 5.14

- raspbian [Greg Annandale Photography Image Licence]
  - buster

- solus [Unsplash License , Pexels License and CC-BY-3.0]
  - 4.1

- ubuntu [CC-BY-SA 3.0]
  - 20.10
  - 20.04
  - 18.04

- antiX [Unknown]
  - 16.1
  - 17.1
  - 19
  
- MX Linux [Unknown]
  - 19

- endeavour [GPL-3.0]
  - 2021.02.03
  
- Fedora [CC-BY-SA-4.0]
  - 33
